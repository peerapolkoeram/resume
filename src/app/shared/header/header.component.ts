import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  open = false;

  constructor( private routor: Router) { }

  ngOnInit(): void {
    
  }

  openNav() {
    console.log("Test");
    var alementOpen = document.getElementById("myNav");
    if(alementOpen) {
      alementOpen.style.width = "100%";
    }
  }
  
  closeNav() {
    var alementClose = document.getElementById("myNav");
    if(alementClose) {
      alementClose.style.width = "0%";
    }
  }

  home() {
    this.routor.navigate(['/home']);
  }
  skill() {
    this.routor.navigate(['/skill']);
  }
  project() {
    this.routor.navigate(['/project']);
  }
  contact() {
    this.routor.navigate(['/contact']);
  }
}
